 apercu-service

This is the web service to serve the apercu study site.  It is based on Python3 and DJango.  
It serves up the html and javascript contained in the ajpercu-build directory, and studies from the apercu-studies directory.  
Both of these directories are at the same level as the ajpercu-service directory.

So a running system from directory x would have the directory structure:

* top-level-directory
    * aprecu-build
    * apercu-studies
    * apercu-service
    * apercu-app

## Directory structure

The apercu is a single page type web app, so most of the action takes place through ajax api calls.  The directories inside of apercu-service are:

| directory | description |
| --- | --- |
| main | the service entry point, top level url dispatcher |
| survey | the application url dispatcher, javascript & html provider |
| | redirects 'dist' routes to the apercu-build directory |
| api | the api handlers |

## Service API

The apercu-app communicates with the apercu-service through api calls.  These are handled in the api/views.py module.  
Arguments are passed in-line with the url, such as:
```
https://host.com/api/get_study_user/?turk_id=34&study_id=234-453
```
The api elements are:

| url | arguments | description |
| --- | --- | --- |
| api/get_study_user | | Registers a user for a study, and returns the 1st instrument to display |
| | turk_id | The turk_id of the study participant |
| | study_id | The study_id of the study being registered |
| report_user_answers | (POST) | Reports the answers for the current instrument |
| | turk_id | The turk_id of the study participant |
| | study_id | The study_id of the study being  |
| | instrument_sequence_id | The instrument_sequence_id of the questions being answered |
| | POST data | The ansewer json |
| import_study | | Import the named study from apercu-studies into the database.  |
| | | A re-load of a study will override the old study for new requests, but does not overwrite the old study results. |
| | study_name | the study to import |
| | load_token | secret token needed to load the study (see container README) |
| get_static_file | | fetch a static file from the study directory, used to fetch images, html of a study |
| | study_dir | study directory name |
| | study_fn | study file name |
| log | (POST) | log a user action |
| | turk_id | The turk_id of the study participant |
| | study_id | The study_id of the study being  |
| | element_id | The element_id of where the interacton takes place |
| | POST data | Metadata of the interaction  |
| wakeup | | To wakeup the service if sleeping |

## Running the service

To run the service locally, these repositories are needed   

* apercu-studies
* apercu-service
* apercu-build
* apercu-app (if you want to make changes to the app)


The 'dist' directory under static should point off to where the bundle.js code exists (with a symbolic link).  It points to the apercu-build directory.
An example directory structure of the static directory using the apercu-buid repository would be:

```
lrwxr-xr-x    dist -> ../../apercu-build
lrwxr-xr-x    images -> ../../apercu-build/images
```

This is how the symbolic links should be stored in the repository.  If you change them to use the apercu-app directory please DO NOT SAVE them to the repository that way,
the container build depends on them being as above.

Note:  The apercu-app 'dist' directory is also sym linked to the apercu-build directory.  So all fresh builds are ready to be served locally, 
and also ready to be pushed back into the repository apercu-build.

## Creating the databases

You must create the database tables for Apercu to run.  You have to create the django and api databases.  Skip this when running locally, 
it is show below, but when running in a container, the database will have to be 
initialized either through a command line in the container, or a local Apercu 
pointing off to the external database using the db_info variable set in settings.py.  

This is done with:

```
python3 ./manage.py makemigrations
python3 ./manage.py migrate
python3 ./manage.py makemigrations api
python3 ./manage.py migrate api
```

## Running locally

Here are the steps you need to take so run apercu locally.  Apercu requires python3, so the first step 
is to setup your python environment (after you have python3 installed).  Apercu expects that you are using a bash shell.

```
python3 -m virtualenv venv

```

Now before running apercu, you must activate your virtual environment using:
```angular2html
source venv/bin/activate
```

Before running apercu the 1st time, you need to pull over the requirements, and setup the database.
To pull the requirements use:

```angular2html
pip install -r requirements.txt
```

Set your db_info in main/settings.py to point to your local database, such as:
```
db_info = 'postgresql://postgres:123456@localhost:32774/surveys'
```

This gets sets up your access to the database.
With your database service running, you are ready to setup the database migrations using:

```
python3 ./manage.py makemigrations
python3 ./manage.py migrate
python3 ./manage.py makemigrations api
python3 ./manage.py migrate api
```

The last step is to run the service.  Look in run.sh to see what environment variables can be set to control your service.
Then run:

```
source ./run.sh
```

The output should look something like:
```
BASE DIR = '/Users/krivacic/Dev/apercu-service'
Use default DB info: postgresql://postgres:123456@localhost:32774/surveys
user: postgres
host: localhost:32774
db: surveys
Performing system checks...

System check identified no issues (0 silenced).
August 07, 2019 - 16:48:48
Django version 2.1.3, using settings 'main.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```


## Environment variables

The following environment variables are used by the apercu service:


| environ var | description |
| ---| --- |
| LOGGING_DATABASE | database access string |
| BROWSER_MIN_WIDTH | minimum acceptable browser width (default 2014)|
| BROWSER_MIN_HEIGHT| minimum acceptable browser height (default 800)|
| AUTOLOAD_STUDY | Study uuid to auto load to.  Forces all to be in the specified study |
| UPLOAD_TOKEN | token needed to upload a study |

