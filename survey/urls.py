from django.urls import path
from django.conf.urls import url, include
from django.views.generic import TemplateView
from . import views

urlpatterns = [
#    url('', TemplateView.as_view(template_name='index.html')),
    url('dist/styles.css', views.style),
    url('dist/bundle.js', views.bundle),
    url('', views.index),
]
