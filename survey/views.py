
import os

from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def index(request):
    logging = request.GET.get('logging', 'on')
    reset_local = request.GET.get('reset_local', 'false')
    dict = {'mode': 'register', 'survey': 'surveyId', 'turkId': '', 'userId': '', 'reset_local':reset_local, 'logging': logging}
    html = render(request, 'index.html', dict)
    return html

@csrf_exempt
def style(request):
	print('get style')
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	print("BASE DIR = '" + BASE_DIR + "'")
	#style = render(request, BASE_DIR + '/frontend/dist/styles.css', content_type="text/css")
	#print(style)
	return ''

@csrf_exempt
def bundle(request):
	print("get bundle")
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	print("BASE DIR = '" + BASE_DIR + "'")
	fullName = BASE_DIR + '/frontend/dist/bundle.js'
	print("File name " + fullName)
	file = open(fullName)
	js = file.read()
	js = render(request, js)
	#js = render(request, BASE_DIR + '/frontend/dist/bundle.js', {'foo':'bar'}, content_type="application/x-javascript")
	#print(js)
	return js
