from django.urls import path
from django.conf.urls import url, include
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url('get_study_user_app_dict', views.get_study_user_app_dict),
    url('get_study_user', views.get_study_user),
    url('report_user_answers', views.report_user_answers),
    url('import_study', views.import_study),
    url('get_static_file', views.get_static_file),
    url('get_environ_info', views.get_environ_info),
    url('wakeup', views.wakeup),
    url('log', views.log),
]
