import datetime
import uuid
from django.db import models
from django.utils import timezone

class Study(models.Model):
    __tablename__ = 'study'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    directory_name = models.CharField(max_length=80)
    name = models.CharField(max_length=200)
    active = models.BooleanField(default=True)
    info = models.CharField(max_length=2048)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name + ": " + str(self.active)

class InstrumentSequence(models.Model):
    __tablename__ = 'instrument_sequence'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    study_id = models.CharField(max_length=200)
    instrument_id = models.CharField(max_length=200)
    sequence = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)   


class Instrument(models.Model):
    __tablename__ = 'instrument'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    meta_data = models.CharField(default='{}', max_length=2048)
    branch_logic = models.CharField(max_length=2048, default='')
    score_logic = models.CharField(max_length=2048, default='')
    timestamp = models.DateTimeField(auto_now_add=True)

class InstrumentPage(models.Model):
    __tablename__ = 'instrument_page'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)
    instrument_id = models.CharField(max_length=200)
    sequence = models.IntegerField(default=0)
    last_page = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)   

    def toJson(self):
        return {
            'uuid': str(self.uuid), 
            'title': self.title, 
            'instrument_id': self.instrument_id, 
            'sequence': self.sequence
        }

class Element(models.Model):
    __tablename__ = 'element'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=80, default='')
    text = models.TextField()
    element_type = models.CharField(max_length=200)
    required = models.BooleanField(default=True)
    choices = models.CharField(max_length=2048)
    meta_data = models.CharField(default='{}', max_length=2048)
    timestamp = models.DateTimeField(auto_now_add=True) 

    def toJson(self):
        choices = []
        if self.choices:
            choices = self.choices
        return {
            'uuid': str(self.uuid),
            'text': self.text,
            'type': self.element_type,
            'required': self.required,
            'meta_data': self.meta_data,
            'name': self.name,
            'choices': choices
        }

class ElementSequence(models.Model):
    __tablename__ = 'instrument_sequence'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    instrument_page_id = models.CharField(max_length=200)
    element_id = models.CharField(max_length=200)
    sequence = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)   

class User(models.Model):
    __tablename__ = 'user'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    turk_id = models.CharField(max_length=200)
    meta_data = models.CharField(max_length=2048, default='')
    black_listed = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.turk_id + ' at ' + self.current_study_id

class StudyUser(models.Model):
    __tablename__ = 'study_user'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    study_id = models.CharField(max_length=200)
    turk_id = models.CharField(max_length=200)
    browser_info = models.CharField(max_length=2048,default='unknown')
    app_dict = models.CharField(max_length=8096,default='{}')
    current_instrument_sequence = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    completion_code = models.CharField(default='', max_length=200)
    paid = models.BooleanField(default=False)
    score = models.IntegerField(default=0)
    paid_date = models.DateTimeField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.study_id + ", " + self.turk_id + ' at ' + str(self.current_instrument_sequence)

    def toJson(self):
        return {
            'uuid': str(self.uuid),
            'study_id': self.study_id,
            'turk_id': self.turk_id,
            'score': self.score,
            'browser_info': self.browser_info,
            'current_instrument_sequence': self.current_instrument_sequence,
            'completed': self.completed,
            'app_dict': self.app_dict,
            'completion_code': self.completion_code
        }

class Answer(models.Model):
    __tablename__ = 'user'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    element_id = models.CharField(max_length=200)
    study_id = models.CharField(max_length=200)
    turk_id = models.CharField(max_length=200)
    answer = models.CharField(max_length=2048)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.element_id + ':  ' + self.answer

class Log(models.Model):
    __tablename__ = 'user'
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    study_id = models.CharField(max_length=200)
    turk_id = models.CharField(max_length=200)
    element_id = models.CharField(max_length=200)
    log_str = models.CharField(max_length=2048)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Log ' + self.element_id + ':  ' + self.log_str

