
import os
import json
from django.shortcuts import render
import pprint
import uuid
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from . import database_io as DIO
from main.settings import STARTUP_INFO
import math
import random

# Create your views here.

from django.http import HttpResponse

def get_1st_in_sequence(key_name, val, list):
    rslt = None
    #print("Get 1st for " + key_name + " = " + str(val))
    for le in list:
        if val == le[key_name]:
            if rslt is None or le['sequence'] < rslt['sequence']:
                rslt = le
    #pprint.pprint(rslt)
    return rslt


def get_all_in_sequence(key_name, val, list):
    rslt = []
    #print("Get all for " + key_name + " = " + val)
    for le in list:
        if val == le[key_name]:
            rslt.append(le)
    #pprint.pprint(rslt)
    return rslt

def find_study_user(list, study_id, turk_id):
    rslt = None
    for su in list:
        if su['study_id'] == study_id and su['turk_id'] == turk_id:
            return su
    return rslt

def get_element(el_id):
    for e in element:
        if e['uuid'] == el_id:
            return e
    return None

def get_instrument(uuid):
    for e in instrument:
        if e['uuid'] == uuid:
            return e
    return None

def build_study_json(study_user):
    rslt = {}
    study = None

    study_user = DIO.get_next_instrument(None, study_user, False, None, 0)

    #print("Net next instrument for Study user " + study_user.turk_id)
    #pprint.pprint(study_user)

    ins_seq = get_1st_in_sequence('uuid', study_user['current_instrument_sequence'], instrument_sequence)
    instrument = get_instrument(ins_seq['instrument_id'])
    #print("Ins back")
    pprint.pprint(ins_seq)
    #print("instrument")
    pprint.pprint(instrument)
    pages = get_all_in_sequence('instrument_id', ins_seq['instrument_id'], instrument_page)
    #print("Pages")
    spages = sorted(pages, key=lambda el: el['sequence'])
    #pprint.pprint(spages)
    for page in spages:
        # gather next elements for instrument sequence
        els = get_all_in_sequence('instrument_page_id', page['uuid'], element_sequence)
        sels = sorted(els, key=lambda el: el['sequence'])
        #print("Sorted els")
        #pprint.pprint(sels)
        els = []
        for sel in sels:
            el = get_element(sel['element_id'])
            if el is not None:
                el['instrument_page_id'] = sel['instrument_page_id']
                els.append(el)
        page['questions'] = els
    #print("Full elements for instrament: " + study_user['current_instrument_sequence'])
    #pprint.pprint(spages)
    rslt = {
        'pages': spages,
        'surveyTitle': instrument['title']
    }
    pprint.pprint(rslt)
    study_user['pageInfo'] = rslt
    return study_user

def set_next_instrument(study_user):
    cis = study_user['current_instrument_sequence']
    iseqs = get_all_in_sequence('study_id', study_user['study_id'], instrument_sequence)
    #print("iseqs")
    #pprint.pprint(iseqs)
    seq_element = get_1st_in_sequence('uuid', cis, iseqs)
    #print("1st")
    #pprint.pprint(seq_element)
    seq_num = seq_element['sequence']
    #print("Current seq num: " + str(seq_num))
    seq_num += 1
    seq_element = get_1st_in_sequence('sequence', seq_num, iseqs)
    if seq_element:
        #print("Found next")
        #pprint.pprint(seq_element)
        study_user['current_instrument_sequence'] = seq_element['uuid']
    else:
        #print("We are done, set completed, stay on thank you page")
        study_user['completed'] = True

def get_study_user(request):
    study_id = request.GET.get('study_id', None)
    turk_id = request.GET.get('turk_id', None)
    browser_info = request.GET.get('browser_info', None)
    #print("get study: " + study_id + ", turk " + turk_id)
    study_user = {}
    if study_id != None and turk_id != None:
        user = DIO.get_user(turk_id)
        if user == None:
            user = DIO.set_user(turk_id)
        if user.black_listed:
            return HttpResponse(json.dumps({"result": "not_now"}), content_type="application/json")

        study = DIO.get_study(study_id)
        if study == None:
            return HttpResponse(json.dumps({"result": "bad_study_id"}), content_type="application/json")

        study_user = DIO.get_study_user(study_id, turk_id)

        if study_user == None:
            print("Create new study user for " + turk_id + " in " + study_id)
            study_user = DIO.set_study_user(study_id, turk_id, browser_info)

        study_user = DIO.get_next_instrument(None, study_user, False, [], 0)
    else:
        return HttpResponse(json.dumps({"result": "bad_user_or_study_id"}), content_type="application/json")

    return HttpResponse(json.dumps(study_user), content_type="application/json")

def get_study_user_app_dict(request):
    study_id = request.GET.get('study_id', None)
    turk_id = request.GET.get('turk_id', None)
    study_user = DIO.get_study_user(study_id, turk_id)
    ad = study_user.app_dict
    #print("get_study_user_app_dict\n" + json.dumps(ad))
    return HttpResponse(json.dumps(ad), content_type="application/json")

@csrf_exempt
@api_view(['POST'])
def report_user_answers(request):
    study_id = request.GET.get('study_id', None)
    turk_id = request.GET.get('turk_id', None)
    nodb = request.GET.get('nodb', "false")
    instrument_sequence_id = request.GET.get('instrument_sequence_id', None)
    lanswers = None
    study_user = None

    try:
        lanswers = json.loads(request.body)
        study_user = DIO.get_study_user(study_id, turk_id)
    except:
        lanswers = None
    nextInx = None
    #pprint.pprint(lanswers)
    answer_dict = lanswers['answer_dict']
    inst_uuid = lanswers['instrument_uuid']
    inst = DIO.get_Instrument(inst_uuid)

    #print("*** Got answers, the branch logic is in: ")
    #print(str(inst))
    #print("Answer dict")
    #print(str(answer_dict))

    if study_user is not None and lanswers is not None and 'pages' in lanswers:

        # log answers into database
        for page in lanswers['pages']:
            for ans in page['elements']:
                #print("Report answers")
                #pprint.pprint(ans)
                try:
                    element_id = ans['uuid']
                    answer = ans['answer']
                    if nodb != "true":
                        DIO.answerToDb(turk_id, study_id, element_id, answer)
                except:
                    element_id = None

        # update score
        if inst.score_logic:
            #print("Have score to check, score: " + str(study_user.score))
            logicStr = inst.score_logic
            #print(logicStr)
            try:
                evalStr = DIO.substitute_values(answer_dict, json.loads(study_user.app_dict), logicStr, study_user.score, random.randint(0,99), 100)
                #print("Score eval: " + evalStr)
                study_user.score = eval(evalStr)
                DIO.update_study_user_score(study_user)
            except:
                print("Error executing score_logic of " + evalStr)

        study_user = DIO.get_next_instrument(inst, study_user, True, answer_dict, study_user.score)
    return HttpResponse(json.dumps(study_user), content_type="application/json")

def import_study(request):
        study_name = request.GET.get('study_name', None)
        load_token = request.GET.get('load_token', None)
        print("Study import name is " + study_name)
        if study_name is not None and load_token is not None and load_token == STARTUP_INFO['upload_token']:
            study_json = DIO.getStudyJson(study_name)
            if (study_json):
                res = DIO.studyJsonToDb(study_name, study_json)
                return HttpResponse('Study ' + study_name + ', upload response: ' + res, content_type="text/html")
            else:
                return HttpResponse('Study ' + study_name + ': JSON decoding error.', content_type="text/html")
        else:
            return HttpResponse('Study name or token error.', content_type="text/html")

def get_static_file(request):
    study_dir = request.GET.get('study_dir', None)
    study_fn = request.GET.get('study_fn', None)
    #print("Study dir is " + study_dir + ", fn " + study_fn)
    try:
        file_object  = open('../apercu-studies/'+study_dir+'/' + study_fn, 'r')
        file_str = file_object.read()
        return HttpResponse(file_str, content_type="text/html")
    except:
        return HttpResponse('', content_type="text/html")

@csrf_exempt
@api_view(['POST'])
def log(request):
    study_id = request.GET.get('study_id', '')
    turk_id = request.GET.get('turk_id', request.GET.get('user_id', ''))

    element_id = request.GET.get('element_id', '')
    body = request.body
    body_json = json.loads(request.body)
    log_str = body_json['dataStr']
    response = ''

    #print("BODY: " + str(body))
    #print("LOG: " + study_id + ', turk: ' + turk_id + ", ele: " + element_id)
    #print('log str ' + log_str)
    try:
        DIO.logToDb(turk_id, study_id, element_id, log_str)
        response = HttpResponse('logged', content_type="text/html")
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "*"
    except:
        return HttpResponse('error', content_type="text/html")

    if element_id.startswith('app_feedback.'):
        try:
            #print("Have app feedback")
            study_user = DIO.get_study_user(study_id, turk_id)
            #print("Study user is " + str(study_user))
            if study_user:
                body_dict = json.loads(log_str)
                if not study_user.app_dict:
                    study_user.app_dict = '{}'
                adict = json.loads(study_user.app_dict)
                #print("Dict before " + str(adict))
                #print("Items: " + str(body_dict))
                prefix = ''
                if 'element_name' in adict:
                    prefix = adict['element_name'] + "."
                if 'element_name' in body_dict:
                    prefix = body_dict['element_name'] + "."
                for key in body_dict.keys():
                    # PUT the item in the dict for this study/turk id
                    if key != 'element_name':
                        adict[prefix+key] = body_dict[key]
                #print("Dict after " + str(adict))
                study_user.app_dict = json.dumps(adict)
                #print("Saving study user dict " + study_user.app_dict)
                DIO.update_study_user_app_dict(study_user)
                #print("Save worked?")
                new_user =  DIO.get_study_user(study_id, turk_id)
                #print("Saved user is " + str(new_user))
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return HttpResponse('error - error saving app_dict', content_type="text/html")

    return response

@csrf_exempt
def wakeup(request):
    return HttpResponse('rslt-'+str(uuid.uuid4()), content_type="text/html")

def get_environ_info(request):
    host = request.get_host()
    #fullHost = request.build_absolute_uri()
    #print("Full host: " + fullHost)
    #trucUrl=fullHost[0:len(fullHost) - len('/api/get_environ_info/')]
    truncUrl = host
    #if request.is_secure():
    if truncUrl.find('heroku') > 0:
        truncUrl = "https://" + truncUrl
    else:
        truncUrl = "http://" + truncUrl
    #print("This Host is " + str(host) + " full uri " + fullHost + ", split: " + truncUrl)
    print("This Host is " + truncUrl)
    info = {
        'host': truncUrl,
        'browser_min_width': STARTUP_INFO['browser_min_width'],
        'browser_min_height': STARTUP_INFO['browser_min_height'],
        'autoload_study': STARTUP_INFO['autoload_study']
    }
    return HttpResponse(json.dumps(info), content_type="application/json")

#get_1st_in_sequence('study_id', 's1', instrument_sequence)
#get_1st_in_sequence('instrument_page_id', 'i11', element_sequence)
#build_study_json('s1', 'u1', studies, study_users, users, instrument_sequence, instrument, element_sequence, element)

