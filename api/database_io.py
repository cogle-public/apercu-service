import datetime
import uuid
from django.db import models
from . import models
import pprint
import json
import random
import urllib

def write_study(name, active, info):
    db_obj = Study(name=name, active=active, info=info)
    db_obj.save()

def get_study(study_id):
    luuid = ''
    try:
        luuid = uuid.UUID(study_id)
    except:
        return None
    study = models.Study.objects.filter(uuid = luuid).first()
    #print("Got study")
    #pprint.pprint(study)
    return study

def get_user(turk_id):
    user = models.User.objects.filter(turk_id = turk_id).first()
    return user

def set_user(turk_id):
    user = models.User(turk_id=turk_id)
    user.save()
    return user

def get_study_info(study_user):
    if study_user:
        #print("get_study_info >>>>>>>>>")
        #print(str(study_user))
        study = models.Study.objects.filter(uuid = study_user['study_id']).first()
        #print("Study info: " + str(study.info))
        study_user['study_info'] = json.loads(study.info)
    return study_user

def get_study_user(study_id, turk_id):
    study_user = models.StudyUser.objects.filter(study_id = study_id, turk_id = turk_id).first()
    return study_user

def set_study_user(study_id, turk_id, browser_info):
    #print("Un encode " + urllib.parse.unquote(browser_info))
    study_user = models.StudyUser(study_id=study_id, turk_id=turk_id, browser_info=urllib.parse.unquote(browser_info), current_instrument_sequence=1, completed=False, completion_code='', paid=False)
    study_user.save()
    return study_user

def update_study_user_score(study_user):
    study_user.save(update_fields=['score'])

def update_study_user_app_dict(study_user):
    study_user.save(update_fields=['app_dict'])

def get_Instrument(inst_uuid):
    inst = models.Instrument.objects.filter(uuid=inst_uuid).first()
    return inst

def substitute_values(answer_dict, app_dict, logicStr, score, r, rand_size):
    #print("Substitute " + str(logicStr) + ', rand_size: ' + str(rand_size))
    answer_dict['random_path'] = r
    #print(str(answer_dict))
    for key in answer_dict.keys():
        #print("Try key: " + key)
        key_match = "{" + key + "}"
        #print("Match key" + str(answer_dict[key]))
        if logicStr.find(key_match) >= 0:
            logicStr = logicStr.replace(key_match, str(answer_dict[key]))
    #print("App Dict: " + str(app_dict))
    for key in app_dict.keys():
        #print("Try key: " + key)
        key_match = "{" + key + "}"
        #print("Match key" + str(app_dict[key]))
        if logicStr.find(key_match) >= 0:
            logicStr = logicStr.replace(key_match, str(app_dict[key]))
    if logicStr.find("{score}") >= 0:
        logicStr = logicStr.replace("{score}", str(score))
    return logicStr

def handle_branch_logic(inst, nextInx, answer_dict, app_dict, score):
        if inst:
            if inst.branch_logic:
                #print("Have branch logic " + inst.branch_logic)
                branch_list = json.loads(inst.branch_logic)
                rsize = 100
                meta_data = {}
                if inst.meta_data:
                    #print("Get meta_data")
                    meta_data = json.loads(inst.meta_data)
                if 'inst_sequence_cnt' in meta_data:
                    rsize = meta_data['inst_sequence_cnt']
                r = random.randint(0, rsize-1)
                #print("Rand " + str(r) + ', rsize: ' + str(rsize))
                for bl in branch_list:
                    if nextInx == None:
                        evalStr = ''
                        try:
                            #print("bl: " + str(bl))
                            evalStr = substitute_values(answer_dict, app_dict, bl['condition'], score, r, rsize)
                            #print("*** test branch str " + evalStr);
                            branch_to = eval(evalStr);
                            #print("*** branch restuls: " + str(branch_to))
                            if branch_to:
                                nextInx = bl['target_inx']
                                #print("*** new branch inx = " + str(nextInx))
                        except:
                            print("Error executing branch_logic of " + evalStr)
        return nextInx

def get_next_instrument(inst, study_user, get_next, answer_dict, score):
    #print("get_next_instrument >>>>>>>>> ")
    seq_num = study_user.current_instrument_sequence
    next_bump = seq_num
    if get_next:
        next_bump = next_bump + 1

    jump_seq = handle_branch_logic(inst, None, answer_dict, json.loads(study_user.app_dict), score)

    if jump_seq:
        next_bump = jump_seq

    #print("Old: " + str(seq_num) + ', new ' + str(next_bump))

    old_seq_element = models.InstrumentSequence.objects.filter(study_id=study_user.study_id, sequence=seq_num).first()
    new_seq_element = models.InstrumentSequence.objects.filter(study_id=study_user.study_id, sequence=(next_bump)).first()
    inst_count = models.InstrumentSequence.objects.filter(study_id=study_user.study_id).all().count()
    #print("Next instrument get_next: " + str(get_next) +  " jump_seq " + str(jump_seq) + " next: " + str(next_bump) + " length: " + str(inst_count))
    #pprint.pprint(study_user)
    #pprint.pprint(old_seq_element)
    #pprint.pprint(new_seq_element)

    set_completed = False

    if new_seq_element:
        #print("Found next " + str(next_bump))
        study_user.current_instrument_sequence = next_bump
        study_user.save(update_fields=['current_instrument_sequence'])
    elif old_seq_element:
        #print("*** We are done, set completed, stay on thank you page ***")
        set_completed = True
        new_seq_element = old_seq_element
    else:
        print("No instruments found")
        study_user = study_user.toJson()
        return study_user
    
    instrument = models.Instrument.objects.filter(uuid=new_seq_element.instrument_id).first()

    if instrument.type == 'random_select' or instrument.type == 'exit':
        return get_next_instrument(instrument, study_user, True, answer_dict, score)

    pages = models.InstrumentPage.objects.filter(instrument_id=new_seq_element.instrument_id).order_by('sequence').all()
    pgs = []

    for page in pages:
        pg = page.toJson()
        pgs.append(pg)
        pg['elements'] = []
        elementsSeq = models.ElementSequence.objects.filter(instrument_page_id = pg['uuid']).order_by('sequence').all()
        for els in elementsSeq:
            ele = models.Element.objects.filter(uuid = els.element_id).first()
            els_json = ele.toJson()
            if els_json['text'].find('*completion_code*') >= 0 or els_json['meta_data'].find('*completion_code*') >= 0 :
                if not study_user.completion_code or len(study_user.completion_code) < 1:
                    study_user.completion_code = str(int(random.random()*2000))+str(uuid.uuid4()).replace('-', '')+str(int(random.random()*2000))
                    #print("New completion code")
                    study_user.save(update_fields=['completion_code'])
                    print("*** completion code created ***")
                    set_completed = True

                els_json['text'] = els_json['text'].replace('*completion_code*', study_user.completion_code)
                els_json['meta_data'] = els_json['meta_data'].replace('*completion_code*', study_user.completion_code)

            pg['elements'].append(els_json)
    inst_inx = study_user.current_instrument_sequence - 1
    if inst_inx < 0:
        inst_inx = 0
    rslt = {
        'pages': pgs,
        'surveyTitle': instrument.title,
        'progress': float(inst_inx) / float(inst_count),
        'instrument_uuid': new_seq_element.instrument_id
    }
    jstudy_user = study_user.toJson()
    if set_completed:
        print("*** set completed ***")
        study_user.completed = True
        study_user.save(update_fields=['completed'])
    jstudy_user['pageInfo'] = rslt
    jstudy_user['result'] = 'OK'
    #print("********************* Oh the final result ****************")
    #pprint.pprint(jstudy_user)
    get_study_info(jstudy_user)
    return jstudy_user

def validElement(ele):
    valid = True
    if ele and ele['text'] and ele['type']:
        tp = ele['type']
        if not 'choices' in ele:
            ele['choices'] = []
        if tp == 'slider':
            valid = valid and len(ele['text']) > 0
        elif tp == 'para' or tp == 'html' or tp == 'text' or tp == 'text_area':
            valid = valid and len(ele['text']) > 0
        elif tp == 'multiple_choice' or tp == 'multiple_select':
            valid = valid and len(ele['choices']) > 0
        else:
            valid = False
    else:
        valid = False
    return valid

def validateStudyJson(studyJson):
    sj = studyJson
    valid = True
    validErrors = []

    #print("Validate json to database")

    if 'name' in sj and 'instruments' in sj and len(sj['instruments']) > 0:
        for inst in sj['instruments']:
            if 'title' in inst and 'pages' in inst and  len(inst['pages']) > 0:
                for page in inst['pages']:
                    if 'title' in page and 'elements' in page and len(page['elements']) > 0:
                        for ele in page['elements']:
                            valid = valid and validElement(ele)
                    else:
                        print("No title or elements")
                        validErrors.append("No title or elements in a page")
                        valid = False
            elif 'title' in inst and 'type' in inst and inst['type'] == 'include' and 'study_dir' in inst and len(inst['study_dir']) > 0:
                print("Including " + inst['study_dir'])
            elif 'title' in inst and 'type' in inst and inst['type'] == 'random' and 'instruments' in inst and len(inst['instruments']) > 1:
                print("Random")
            else:
                print("No title or pages")
                validErrors.append("No title or pages in an instrument")
                valid = False
    else:
        print("No name or instruments")
        validErrors.append("No name or instruments")
        valid = False

    #print("Valid: " + str(valid))
    return validErrors

def getStudyJson(study_dir):
    file_object  = open('../apercu-studies/'+study_dir+'/study.json', 'r')
    #print("Reading study json at " + '../apercu-studies/'+study_dir+'/study.json')
    file_str = file_object.read()
    #print("Got str " + file_str)
    try:
        study_json = json.loads(file_str)
        return study_json
    except json.JSONDecodeError:
        print("Error in Json for study: " + study_dir)
        return None   

def studyInstrumentsPass1(active_prefix, study_uuid, instruments, inst_inx, inst_entry_name_to_inst):
    sj_uuid = study_uuid

    #
    # Pass1 sets up the instrument sequence indexes into the json,
    # and creates the instrument name table
    #

    for inst in instruments:
        inst_entry_name = active_prefix;
        if 'name' in inst and len(inst['name']) > 0:
            inst_entry_name = active_prefix + inst['name']
        inst['inst_prefix'] = active_prefix
        if 'type' in inst and inst['type'] == 'include':
            print("Included study dir: " + active_prefix +  ', name: ' + inst_entry_name)
            includeJson = getStudyJson(inst['study_dir'])
            #print("JSON: " + json.dumps(includeJson))
            inst['include_json'] = includeJson
            inst['inst_entry_name'] = inst_entry_name
            inst_inx = studyInstrumentsPass1(inst_entry_name + '_' + str(inst_inx) + '/', study_uuid, includeJson['instruments'], inst_inx, inst_entry_name_to_inst)
        else:
            inst_inx += 1
            inst['sequence_inx'] = inst_inx
            inst['inst_entry_name'] = inst_entry_name
            inst_entry_name_to_inst[inst_entry_name] = inst_inx
            print("Set name: " + inst_entry_name + " to index " + str(inst_inx))
            # reserve space for the random children instruments & exit instrument

            inst_exit_name = inst_entry_name + "_exit"
            print("Exit name")

            if 'type' in inst and inst['type'] == 'random':
                instrumentJson = []
                entryInxs = []
                entryLabels = []
                mySeqs = []
                getSequence([], len(inst['instruments']), mySeqs)
                inst['sequences'] = mySeqs
                print("my sequences " + json.dumps(mySeqs))
                # Find out how
                for seqList in mySeqs:
                    # insert a copy of the json for each instrument in the sequence
                    entryInxs.append(inst_inx+1)
                    #print("At : " + str(inst_inx) + " add sequence: " + json.dumps(seqList))
                    jsonList = []
                    for instInx in seqList:
                        jsonCopy = json.loads(json.dumps(inst['instruments'][instInx]))
                        jsonList.append(jsonCopy)

                    jsonList.append({"title": "exit from seq: " + str(inst_inx+1),
                        "type": "exit",
                        "name": "Exit_"+ inst_entry_name + '_' + str(inst['sequence_inx']) + '/' + str(len(entryInxs)) + '/' + str(inst_inx+1),
                        "branch_logic": [{"condition": "True", "target": inst_exit_name}],
                        "pages": []
                        })

                    #print("Add the sequence json: " + json.dumps(jsonList))
                    instrumentJson.append(jsonList)
                    instPrefix = inst_entry_name + '_' + str(inst['sequence_inx']) + '/' + str(len(entryInxs)) + '/'
                    #entryDir = study_dir + '_' + str(inst_inx)
                    #entryName = inst_entry_name + '_' + str(inst['sequence_inx']) + '/'
                    print("Adding items, 1st one instPrefix: " + instPrefix )
                    #print(json.dumps(jsonList[0]))
                    entryLabels.append(instPrefix)
                    inst_inx = studyInstrumentsPass1(instPrefix,
                        study_uuid,
                        jsonList,
                        inst_inx,
                        inst_entry_name_to_inst)

                exitInst = {"title": "exit from " + instPrefix,
                    "type": "exit",
                    "name": "_exit",
                    "pages": [],
                    }
                print("Generate exit inst " + json.dumps(exitInst))
                inst['exitInst'] = exitInst
                inst_inx = studyInstrumentsPass1(inst_entry_name,
                                                   study_uuid,
                                                   [exitInst],
                                                   inst_inx,
                                                   inst_entry_name_to_inst)

                inst['inst_json'] = instrumentJson
                print("Entry indexes: " + json.dumps(entryInxs))
                inst['entry_inxs'] = entryInxs
                inst['entry_labels'] = entryLabels
                print(json.dumps(entryLabels))
                #print(json.dumps(instrumentJson))
                #inst['inst_sequence_cnt'] = len(inst['sequences'])*len(inst['instruments'])
                #print("Random inst")
                #print(json.dumps(inst))

    #print("Result of Pass 1 of " + study_dir)
    #print(str(instruments))
    return inst_inx


def getSequence(currentSeq, seqLen, rsltCopies):
    #build up copies of the random orderings that the instruments get selected ferom
    for nextSeq in range(0, seqLen):
        rsltSeq = currentSeq.copy()
        if not nextSeq in rsltSeq:
            #print ("item: " + str(nextSeq) + " not in " + json.dumps(rsltSeq))
            rsltSeq.append(nextSeq)
            if len(rsltSeq) < seqLen:
                getSequence(rsltSeq, seqLen, rsltCopies)
            else:
                rsltCopies.append(rsltSeq)

def studyInstrumentsToDb(study_dir, active_prefix, study_uuid, instruments, inst_inx, inst_entry_name_to_inst):
    sj_uuid = study_uuid

    if inst_inx < 1:
        print("**** Name to inx table ****")
        print(json.dumps(inst_entry_name_to_inst))
        print("****  ****")

    #
    # inst name table ...
    #   OR run through json once, creating study_dir.name to inst_inx dictionary
    #   then re-run so name substitution to index can take place!
    #   get the name from json,
    #   add 'study_dir.name' to an InstrumentSequence
    #   in the equation space, substitute jump to name with 'study_dir.name'
    #
    # for named questions (answers)
    #
    for inst in instruments:
        if 'type' in inst and inst['type'] == 'include':
            inst_entry_name = inst['inst_entry_name'];
            if 'name' in inst and len(inst['name']) > 0:
                inst_entry_name = study_prefix + inst['name']
            #print("Have an included study")
            includeJson = inst['include_json']

            inst_inx = studyInstrumentsToDb(inst['study_dir'], inst['inst_prefix'], study_uuid, includeJson['instruments'], inst_inx, inst_entry_name_to_inst)
        elif 'type' in inst and inst['type'] == 'random':
            entry_names = []
            branch_logic = []
            for ix in range(0, len(inst['entry_inxs'])):
                my_entry_name = inst['entry_labels'][ix]
                entry_names.append(my_entry_name)
                branch_logic.append({"condition": '{random_path} == '+str(ix), "target": my_entry_name, "target_inx": inst['entry_inxs'][ix]})

            print(json.dumps(branch_logic))
            # insert the random head instrument
            inst_inx += 1
            meta_data = {"inst_sequence_cnt":  len(inst['entry_inxs'])}
            instrument = models.Instrument(title = inst['title'], branch_logic = json.dumps(branch_logic), score_logic = '', type = 'random_select', meta_data = json.dumps(meta_data))
            instrument.save()
            page_inx = 0
            inst_sq = models.InstrumentSequence(study_id = sj_uuid, instrument_id = instrument.uuid, sequence = inst['sequence_inx'])
            inst_sq.save()

            # insert sequences

            for instPath in inst['inst_json']:
                prefix = inst['inst_prefix'] + inst['name']
                print("Insert with prefix " + prefix)
                print(instPath)
                inst_inx = studyInstrumentsToDb(study_dir, prefix, study_uuid, instPath, inst_inx, inst_entry_name_to_inst)

            # insert the trailing exit no-op instrument
            inst_inx = studyInstrumentsToDb(study_dir, inst['inst_prefix'], study_uuid, [inst['exitInst']], inst_inx, inst_entry_name_to_inst)

        else:
            inst_inx += 1
            branch_logic = []
            if inst.get('branch_logic'):
                branch_logic = inst['branch_logic']
                # loop through branch logic here to substitute the target with the index of the target
                # if there are '/' in the target goto, that is the lookup name
                # else add the study_prefix to the target and look that up
                # branch_logic is the form: [{condition: 'logic-str', 'target': target-instrument-name},...{}]

                for bl in branch_logic:
                    lookup_str = bl['target']
                    if not '/' in lookup_str:
                        lookup_str = inst['inst_prefix'] + lookup_str
                    if lookup_str in inst_entry_name_to_inst:
                        bl['target_inx'] = inst_entry_name_to_inst[lookup_str]
                    else:
                        print("Missing lookup " + lookup_str + " entry: " + (inst['inst_entry_name'] if 'inst_entry_name' in inst else '-none-'))
                        print("Active_prefix " + active_prefix)
                        print(json.dumps(inst))
                        print(inst_entry_name_to_inst)

            ds = json.dumps(branch_logic)

            #print("Result branch logic: " + ds + ' type is ' + str(type(ds)))

            sc = ''
            if inst.get('score_logic'):
                sc = inst.get('score_logic')

            instType = ''
            if 'type' in inst:
                instType = inst['type']
            instrument = models.Instrument(title = inst['title'], type = instType, branch_logic = ds, score_logic = sc)
            instrument.save()
            page_inx = 0
            # add 'study_dir.name' to an InstrumentSequence
            inst_sq = models.InstrumentSequence(study_id = sj_uuid, instrument_id = instrument.uuid, sequence = inst_inx)
            inst_sq.save()
            for page in inst['pages']:
                page_inx += 1
                isLastPage = False
                if 'last_page' in page:
                    isLastPage = page['last_page']
                ip = models.InstrumentPage(title = page['title'], instrument_id = instrument.uuid, sequence = page_inx, last_page = isLastPage)
                ip.save()
                ele_inx = 0
                for ele in page['elements']:
                    ele_inx += 1
                    choices = ''
                    if 'choices' in ele:
                        choices = ele['choices']
                    meta_data = {}
                    if 'meta_data' in ele:
                        meta_data = ele['meta_data']
                    meta_data['study_dir'] = study_dir
                    #pprint.pprint(ele)
                    if not ('required' in ele):
                        ele['required'] = True
                    name = ''
                    if 'name' in ele:
                        name = ele['name']

                    el = models.Element(text = ele['text'], element_type = ele['type'], meta_data = json.dumps(meta_data), name = name, choices = json.dumps(choices), required = ele['required'])
                    print("Saving element: " + str(el.toJson()))
                    el.save()
                    els = models.ElementSequence(instrument_page_id = ip.uuid, element_id = el.uuid, sequence = ele_inx)
                    print("Saving els: " + str(els))
                    els.save()
    return inst_inx

def studyJsonToDb(study_id, studyJson):
    sj = studyJson
    study_uuid = ''
    validErrors = validateStudyJson(sj)
    if len(validErrors) == 0:
        print("Adding json to database for study: " + study_id)
        study = models.Study(directory_name = study_id, name = sj['name'], info = json.dumps(sj['info']))
        study.save()
        study_uuid = study.uuid
        inst_inx = 0
        inst_entry_name_to_inst = {}
        study_idp = study_id + '/'
        studyInstrumentsPass1(study_idp, study.uuid, sj['instruments'], inst_inx, inst_entry_name_to_inst)
        studyInstrumentsToDb(study_id, study_idp, study.uuid, sj['instruments'], inst_inx, inst_entry_name_to_inst)
    else:
        return "Validation errors: " + str(validErrors) + '.'           
    return "Study id: '" + str(study_uuid) + "' uploaded."

def logToDb(turk_id, study_id, element_id, log_str):
    log = models.Log(turk_id = turk_id, study_id = study_id, element_id = element_id, log_str = log_str)
    log.save()

def answerToDb(turk_id, study_id, element_id, answer):
    ans = models.Answer(turk_id = turk_id, study_id = study_id, element_id = element_id, answer = answer)
    ans.save()

mySeqs = []
getSequence([], 4, mySeqs)
print("resuolts")
print(json.dumps(mySeqs))